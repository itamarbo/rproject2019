setwd("C:/xampp/htdocs/RProject")
bankrupt.raw<-read.csv('project.csv',header=TRUE)
str(bankrupt.raw)
summary(bankrupt.raw)

bankrupt <- bankrupt.raw
str(bankrupt)
summary(bankrupt)

#Turn columns to numeric 
bankrupt[] <- lapply(bankrupt, function(x) as.numeric(as.character(x)))

#return if empty rows
empty.data<- function(x){
  check.data<- apply(x,2,function(x)any(is.na(x)))
  return(check.data)
}
empty.data(bankrupt.raw)

#Change NA to average value 
for(i in 1:ncol(bankrupt)){
  bankrupt[is.na(bankrupt[,i]), i] <- mean(bankrupt[,i], na.rm = TRUE)
}
#Change NA to average value for one column

make_average <- function(x,xvec){
  if(is.na(x)){
    return (mean(xvec, na.rm=TRUE))
  }
  return (x)
}

bankrupt$Attr23 <- 
  sapply(bankrupt$Attr23,make_average, xvec = bankrupt$Attr23 )

bankrupt$class <- as.factor(bankrupt.raw$class)
bankrupt$id <- NULL



#-----------------corellation recommand remove------------------------------------------
df_cor <- cor(bankrupt[1:(ncol(bankrupt)-30)], use = "complete.obs")
df_cor


#install.packages("corrplot")
library(corrplot)
corrplot(df_cor, type="upper")


#install.packages("caret")
library(caret)
hc <- findCorrelation(df_cor, cutoff=0.8, verbose = T)
hc = sort(hc)
hc

#High correlation=>(26-34 )(19-23)(1-10)(4-16)(26-34)
#Percent of missing value

#install.packages("VIM")
library("VIM")
aggr(bankrupt)

#remove Attr 37. a lot of missing value
library(ggplot2)



#Check attribute 1-64

#Attr1
bankrupt$Attr1<- as.integer(bankrupt$Attr1)
ggplot(bankrupt, aes(class ,Attr1)) + geom_boxplot()

#Attr2
bankrupt$Attr2<- as.integer(bankrupt$Attr2)
ggplot(bankrupt, aes(class ,Attr2)) + geom_boxplot()

#Attr3
bankrupt$Attr3<- as.integer(bankrupt$Attr3)
ggplot(bankrupt, aes(class ,Attr3)) + geom_boxplot()

#Attr4
bankrupt$Attr4<- as.integer(bankrupt$Attr4)
ggplot(bankrupt, aes(class ,Attr4)) + geom_boxplot()

#Attr5
bankrupt$Attr5<- as.integer(bankrupt$Attr5)
ggplot(bankrupt, aes(class ,Attr5)) + geom_boxplot()

#Attr6
bankrupt$Attr6<- as.integer(bankrupt$Attr6)
ggplot(bankrupt, aes(class ,Attr6)) + geom_boxplot()

#Attr7
bankrupt$Attr7<- as.integer(bankrupt$Attr7)
ggplot(bankrupt, aes(class ,Attr7)) + geom_boxplot()

#Attr8
str(bankrupt$Attr8)
summary(bankrupt$Attr8)
bankrupt$Attr8<- as.integer(bankrupt$Attr8)
ggplot(bankrupt, aes(class ,Attr8)) + geom_boxplot()

#Attr9
bankrupt$Attr9<- as.integer(bankrupt$Attr9)
ggplot(bankrupt, aes(class ,Attr9)) + geom_boxplot()
#no connection in this column

#Attr10
str(bankrupt$Attr10)
summary(bankrupt$Attr10)
bankrupt$Attr10<- as.integer(bankrupt$Attr10)
ggplot(bankrupt, aes(class ,Attr10)) + geom_boxplot()

#Attr11
bankrupt$Attr11<- as.integer(bankrupt$Attr11)
ggplot(bankrupt, aes(class ,Attr11)) + geom_boxplot()

#Attr12
bankrupt$Attr12<- as.integer(bankrupt$Attr12)
ggplot(bankrupt, aes(class ,Attr12)) + geom_boxplot()

#Attr13
str(bankrupt.raw$Attr13)
ggplot(bankrupt, aes(x=Attr13)) + geom_histogram() + xlim(-1,1)

cleanUnusual <- function(x,value){
  if (abs(x)>value) return(value)
  return(x)
}

bankrupt$Attr13 <- sapply(bankrupt$Attr13,cleanUnusual, value=0.5)
ggplot(bankrupt, aes(x=Attr13)) + geom_histogram() + xlim(-1,1)
ggplot(bankrupt, aes(class ,Attr13)) + geom_boxplot()

#Attr14
bankrupt$Attr14<- as.integer(bankrupt$Attr14)
ggplot(bankrupt, aes(class ,Attr14)) + geom_boxplot()

#Attr15
bankrupt$Attr15<- as.integer(bankrupt$Attr15)
ggplot(bankrupt, aes(class ,Attr15)) + geom_boxplot()

#Attr16
bankrupt$Attr16<- as.integer(bankrupt$Attr16)
ggplot(bankrupt, aes(class ,Attr16)) + geom_boxplot()

#Attr17
bankrupt$Attr17<- as.integer(bankrupt$Attr17)
ggplot(bankrupt, aes(class ,Attr17)) + geom_boxplot()

#Attr18
str(bankrupt$Attr18)
summary(bankrupt$Attr18)
ggplot(bankrupt, aes(class ,Attr19)) + geom_boxplot()
ggplot(bankrupt, aes(x=Attr19)) + geom_histogram() + xlim(-500,60)
ggplot(bankrupt, aes(x=Attr19)) + geom_histogram() + xlim(-100,10)
ggplot(bankrupt, aes(x=Attr19)) + geom_histogram() + xlim(-20,5)
bankrupt$Attr18 <- sapply(bankrupt$Attr18,cleanUnusual, value=5)
ggplot(bankrupt, aes(class ,Attr18)) + geom_boxplot()

#Attr19
ggplot(bankrupt, aes(class ,Attr19)) + geom_boxplot()
ggplot(bankrupt, aes(x=Attr19)) + geom_histogram() + xlim(-311,78)
ggplot(bankrupt, aes(x=Attr19)) + geom_histogram() + xlim(-2,2)
ggplot(bankrupt, aes(x=Attr19)) + geom_histogram() + xlim(-1,1)
bankrupt$Attr19 <- sapply(bankrupt$Attr19,cleanUnusual, value=0.5)
ggplot(bankrupt, aes(x=Attr19)) + geom_histogram() + xlim(-1,1)
ggplot(bankrupt, aes(class ,Attr19)) + geom_boxplot()

#Attr20
bankrupt$Attr20<- as.integer(bankrupt$Attr20)
ggplot(bankrupt, aes(class ,Attr20)) + geom_boxplot()

#Check attribute 21-44
coercex <- function(x,by){
  if(x<=by) return(x) 
  return(by)
}
coercexbig <- function(x,by){
  if(x>=by) return(x) 
  return(by)
}


#Attr21
summary(bankrupt.raw$Attr21)
ggplot(bankrupt, aes(x=Attr19)) + geom_histogram() + xlim(-150,8000)
ggplot(bankrupt, aes(x=Attr19)) + geom_histogram() + xlim(-10,50)
ggplot(bankrupt, aes(class ,Attr21)) + geom_boxplot()
bankrupt$Attr21 <- sapply(bankrupt$Attr21,coercex, by=5)
bankrupt$Attr21 <- sapply(bankrupt$Attr21,coercexbig, by=-5)


#Attr22
summary(bankrupt.raw$Attr22)
ggplot(bankrupt, aes(x=Attr22)) + geom_histogram() + xlim(-450,17)
ggplot(bankrupt, aes(x=Attr19)) + geom_histogram() + xlim(-10,5)
bankrupt$Attr22 <- sapply(bankrupt$Attr22,coercex, by=2)
bankrupt$Attr22 <- sapply(bankrupt$Attr22,coercexbig, by=-2)
ggplot(bankrupt, aes(class ,Attr22)) + geom_boxplot()


#Attr23
summary(bankrupt.raw$Attr22)
ggplot(bankrupt, aes(x=Attr22)) + geom_histogram() + xlim(-450,17)
ggplot(bankrupt, aes(x=Attr19)) + geom_histogram() + xlim(-10,5)
bankrupt$Attr22 <- sapply(bankrupt$Attr22,coercex, by=2)
bankrupt$Attr22 <- sapply(bankrupt$Attr22,coercexbig, by=-2)
ggplot(bankrupt, aes(class ,Attr23)) + geom_boxplot()


#Attr24
bankrupt$Attr24<- as.integer(bankrupt$Attr44)
ggplot(bankrupt, aes(class ,Attr24)) + geom_boxplot()


#Attr25
summary(bankrupt.raw$Attr22)
ggplot(bankrupt, aes(x=Attr22)) + geom_histogram() + xlim(-450,17)
ggplot(bankrupt, aes(x=Attr19)) + geom_histogram() + xlim(-10,5)
bankrupt$Attr22 <- sapply(bankrupt$Attr22,coercex, by=2)
bankrupt$Attr22 <- sapply(bankrupt$Attr22,coercexbig, by=-2)
ggplot(bankrupt, aes(class ,Attr25)) + geom_boxplot()


#Attr26
bankrupt$Attr26<- as.integer(bankrupt$Attr26)
ggplot(bankrupt, aes(class ,Attr26)) + geom_boxplot()
#no diffrence


#Attr27
summary(bankrupt.raw$Attr22)
ggplot(bankrupt, aes(x=Attr22)) + geom_histogram() + xlim(-450,17)
ggplot(bankrupt, aes(x=Attr19)) + geom_histogram() + xlim(-10,5)
bankrupt$Attr22 <- sapply(bankrupt$Attr22,coercex, by=2)
bankrupt$Attr22 <- sapply(bankrupt$Attr22,coercexbig, by=-2)
ggplot(bankrupt, aes(class ,Attr27)) + geom_boxplot()


#Attr28
summary(bankrupt.raw$Attr22)
ggplot(bankrupt, aes(x=Attr22)) + geom_histogram() + xlim(-450,17)
ggplot(bankrupt, aes(x=Attr19)) + geom_histogram() + xlim(-10,5)
bankrupt$Attr22 <- sapply(bankrupt$Attr22,coercex, by=2)
bankrupt$Attr22 <- sapply(bankrupt$Attr22,coercexbig, by=-2)
ggplot(bankrupt, aes(class ,Attr28)) + geom_boxplot()


#Attr29
bankrupt$Attr29<- as.integer(bankrupt$Attr29)
ggplot(bankrupt, aes(class ,Attr29)) + geom_boxplot()


#Attr30
bankrupt$Attr30<- as.integer(bankrupt$Attr30)
ggplot(bankrupt, aes(class ,Attr30)) + geom_boxplot()
#no diffrence


#Attr31
summary(bankrupt.raw$Attr31)
ggplot(bankrupt, aes(x=Attr31)) + geom_histogram() + xlim(-330,78)
ggplot(bankrupt, aes(x=Attr31)) + geom_histogram() + xlim(-1,1)
bankrupt$Attr31 <- sapply(bankrupt$Attr31,coercex, by=1)
bankrupt$Attr31 <- sapply(bankrupt$Attr31,coercexbig, by=-1)
ggplot(bankrupt, aes(class ,Attr31)) + geom_boxplot()
#no difffrence


#Attr32
summary(bankrupt.raw$Attr32)
ggplot(bankrupt, aes(x=Attr32)) + geom_histogram() + xlim(-250,50000)
ggplot(bankrupt, aes(x=Attr32)) + geom_histogram() + xlim(-0.05,0.02)
bankrupt$Attr32 <- sapply(bankrupt$Attr32,coercex, by=0.5)
bankrupt$Attr32 <- sapply(bankrupt$Attr32,coercexbig, by=-1)
ggplot(bankrupt, aes(class ,Attr32)) + geom_boxplot()


#Attr33
summary(bankrupt.raw$Attr33)
ggplot(bankrupt, aes(x=Attr33)) + geom_histogram() + xlim(-2,40)
bankrupt$Attr33 <- sapply(bankrupt$Attr33,coercex, by=20)
ggplot(bankrupt, aes(class ,Attr33)) + geom_boxplot()


#Attr34
summary(bankrupt.raw$Attr34)
ggplot(bankrupt, aes(x=Attr34)) + geom_histogram() + xlim(-17,50)
bankrupt$Attr34 <- sapply(bankrupt$Attr34,coercex, by=20)
ggplot(bankrupt, aes(class ,Attr34)) + geom_boxplot()


#Attr35
bankrupt$Attr35<- as.integer(bankrupt$Attr35)
ggplot(bankrupt, aes(class ,Attr35)) + geom_boxplot()
#no diffrence


#Attr36
summary(bankrupt.raw$Attr36)
ggplot(bankrupt, aes(x=Attr36)) + geom_histogram() + xlim(0,1000)
ggplot(bankrupt, aes(x=Attr36)) + geom_histogram() + xlim(0,10)
bankrupt$Attr36 <- sapply(bankrupt$Attr36,coercex, by=5)
ggplot(bankrupt, aes(class ,Attr36)) + geom_boxplot()


#Attr38
summary(bankrupt.raw$Attr38)
ggplot(bankrupt, aes(x=Attr38)) + geom_histogram() + xlim(-80,470)
ggplot(bankrupt, aes(x=Attr38)) + geom_histogram() + xlim(-1,5)
bankrupt$Attr38 <- sapply(bankrupt$Attr38,coercexbig, by=0)
ggplot(bankrupt, aes(class ,Attr38)) + geom_boxplot()
#no diffrence


#Attr39
summary(bankrupt.raw$Attr39)
ggplot(bankrupt, aes(x=Attr39)) + geom_histogram() + xlim(-50,3)
ggplot(bankrupt, aes(x=Attr39)) + geom_histogram() + xlim(-2,5)
bankrupt$Attr39 <- sapply(bankrupt$Attr39,coercexbig, by=-0.3)
bankrupt$Attr39 <- sapply(bankrupt$Attr39,coercexbig, by=0.3)
ggplot(bankrupt, aes(class ,Attr39)) + geom_boxplot()
#no connection


#Attr40
summary(bankrupt.raw$Attr39)
ggplot(bankrupt, aes(x=Attr39)) + geom_histogram() + xlim(-50,3)
ggplot(bankrupt, aes(x=Attr39)) + geom_histogram() + xlim(-2,5)
bankrupt$Attr39 <- sapply(bankrupt$Attr39,coercexbig, by=-0.3)
bankrupt$Attr39 <- sapply(bankrupt$Attr39,coercexbig, by=0.3)
ggplot(bankrupt, aes(class ,Attr40)) + geom_boxplot()


#Attr41
summary(bankrupt.raw$Attr41)
ggplot(bankrupt, aes(x=Attr41)) + geom_histogram() + xlim(-270,5000)
ggplot(bankrupt, aes(x=Attr41)) + geom_histogram() + xlim(-2,50)
ggplot(bankrupt, aes(x=Attr41)) + geom_histogram() + xlim(-2,5)
bankrupt$Attr41 <- sapply(bankrupt$Attr41,coercexbig, by=-0.2)
bankrupt$Attr41 <- sapply(bankrupt$Attr41,coercex, by=1)
ggplot(bankrupt, aes(class ,Attr41)) + geom_boxplot()


#Attr42
summary(bankrupt.raw$Attr42)
ggplot(bankrupt, aes(x=Attr42)) + geom_histogram() + xlim(-150,5)
ggplot(bankrupt, aes(x=Attr42)) + geom_histogram() + xlim(-1,20)
ggplot(bankrupt, aes(x=Attr42)) + geom_histogram() + xlim(-1,2)
bankrupt$Attr42 <- sapply(bankrupt$Attr42,coercexbig, by=-0.2)
bankrupt$Attr42 <- sapply(bankrupt$Attr42,coercex, by=0.5)
ggplot(bankrupt, aes(class ,Attr42)) + geom_boxplot()


#Attr43
summary(bankrupt.raw$Attr43)
ggplot(bankrupt, aes(x=Attr43)) + geom_histogram() + xlim(-150,400)
ggplot(bankrupt, aes(x=Attr42)) + geom_histogram() + xlim(-1,20)
ggplot(bankrupt, aes(x=Attr42)) + geom_histogram() + xlim(-1,2)
bankrupt$Attr43 <- sapply(bankrupt$Attr43,coercex, by=400)
ggplot(bankrupt, aes(class ,Attr43)) + geom_boxplot()
#no diffrent


#Attr44
summary(bankrupt.raw$Attr44)
ggplot(bankrupt, aes(x=Attr44)) + geom_histogram() + xlim(-400,700)
bankrupt$Attr44 <- sapply(bankrupt$Attr43,coercex, by=250)
ggplot(bankrupt, aes(class ,Attr44)) + geom_boxplot()
#no diffrent

#Attr45
str(bankrupt$Attr45)
bankrupt$Attr45 <- as.integer(bankrupt$Attr45)
summary(bankrupt$Attr45)
ggplot(bankrupt, aes(class ,Attr45)) + geom_boxplot()


#Attr46
str(bankrupt$Attr46)
bankrupt$Attr46 <- as.integer(bankrupt$Attr46)
summary(bankrupt$Attr46)
ggplot(bankrupt, aes(class ,Attr46)) + geom_boxplot()


#Attr47
str(bankrupt$Attr47)
bankrupt$Attr47<- as.integer(bankrupt$Attr47)
summary(bankrupt$Attr47)
ggplot(bankrupt, aes(class ,Attr47)) + geom_boxplot()


#Attr48
str(bankrupt$Attr48)
bankrupt$Attr48 <- as.integer(bankrupt$Attr48)
summary(bankrupt$Attr48)
ggplot(bankrupt, aes(class ,Attr48)) + geom_boxplot()


#Attr49
str(bankrupt$Attr49)
summary(bankrupt$Attr49)
ggplot(bankrupt, aes(x=Attr49)) + geom_histogram() + xlim(-25,10)
bankrupt$Attr49 <- sapply(bankrupt$Attr49,coercexbig, by=0.5)
bankrupt$Attr49 <- sapply(bankrupt$Attr49,coercex, by=-1)
ggplot(bankrupt, aes(class ,Attr49)) + geom_boxplot()


#Attr50
str(bankrupt$Attr50)
bankrupt$Attr50 <- as.integer(bankrupt$Attr50)
summary(bankrupt$Attr50)
ggplot(bankrupt, aes(class ,Attr50)) + geom_boxplot()


#Attr51
str(bankrupt$Attr51)
bankrupt$Attr51 <- as.integer(bankrupt$Attr51)
summary(bankrupt$Attr51)
ggplot(bankrupt, aes(class ,Attr51)) + geom_boxplot()


#Attr52
str(bankrupt$Attr52)
bankrupt$Attr52 <- as.integer(bankrupt$Attr52)
summary(bankrupt$Attr52)
ggplot(bankrupt, aes(class ,Attr52)) + geom_boxplot()


#Attr53
str(bankrupt$Attr53)
bankrupt$Attr53 <- as.integer(bankrupt$Attr53)
summary(bankrupt$Attr53)
ggplot(bankrupt, aes(class ,Attr53)) + geom_boxplot()


#Attr54
str(bankrupt$Attr54)
bankrupt$Attr54 <- as.integer(bankrupt$Attr54)
summary(bankrupt$Attr54)
ggplot(bankrupt, aes(class ,Attr54)) + geom_boxplot()


#Attr55
str(bankrupt$Attr55)
summary(bankrupt$Attr55)
ggplot(bankrupt, aes(x=Attr55)) + geom_histogram() + xlim(-10000,25000)
bankrupt$Attr55 <- sapply(bankrupt$Attr55,coercexbig, by=20000)
bankrupt$Attr55 <- sapply(bankrupt$Attr55,coercex, by=-9000)
ggplot(bankrupt, aes(class ,Attr55)) + geom_boxplot()


#Attr56
str(bankrupt$Attr56)
summary(bankrupt$Attr56)
ggplot(bankrupt, aes(x=Attr56)) + geom_histogram() + xlim(-20,25)
bankrupt$Attr56 <- sapply(bankrupt$Attr56,coercexbig, by=1)
bankrupt$Attr56 <- sapply(bankrupt$Attr56,coercex, by=-0.2)
ggplot(bankrupt, aes(class ,Attr56)) + geom_boxplot()



#Attr57
str(bankrupt$Attr57)
bankrupt$Attr57 <- as.integer(bankrupt$Attr57)
summary(bankrupt$Attr57)
ggplot(bankrupt, aes(class ,Attr57)) + geom_boxplot()



#Attr58
str(bankrupt$Attr58)
summary(bankrupt$Attr58)
ggplot(bankrupt, aes(x=Attr58)) + geom_histogram() + xlim(-2,30)
bankrupt$Attr58 <- sapply(bankrupt$Attr58,coercexbig, by=1.5)
ggplot(bankrupt, aes(class ,Attr58)) + geom_boxplot()


#Attr59
str(bankrupt$Attr59)
bankrupt$Attr59<- as.integer(bankrupt$Attr59)
summary(bankrupt$Attr59)
ggplot(bankrupt, aes(class ,Attr59)) + geom_boxplot()


#Attr60
str(bankrupt$Attr60)
bankrupt$Attr60 <- as.integer(bankrupt$Attr60)
summary(bankrupt$Attr60)
ggplot(bankrupt, aes(class ,Attr60)) + geom_boxplot()


#Attr61
str(bankrupt$Attr61)
bankrupt$Attr61 <- as.integer(bankrupt$Attr61)
summary(bankrupt$Attr61)
ggplot(bankrupt, aes(class ,Attr61)) + geom_boxplot()


#Attr62
str(bankrupt$Attr62)
summary(bankrupt$Attr62)
ggplot(bankrupt, aes(x=Attr62)) + geom_histogram() + xlim(-50,1000)
bankrupt$Attr62 <- sapply(bankrupt$Attr58,coercexbig, by=300)
ggplot(bankrupt, aes(class ,Attr62)) + geom_boxplot()


#Attr63
str(bankrupt$Attr63)
bankrupt$Attr63<- as.integer(bankrupt$Attr63)
summary(bankrupt$Attr63)
ggplot(bankrupt, aes(class ,Attr63)) + geom_boxplot()


#Attr64
str(bankrupt$Attr64)
bankrupt$Attr64<- as.integer(bankrupt$Attr64)
summary(bankrupt$Attr64)
ggplot(bankrupt, aes(class ,Attr64)) + geom_boxplot()


#Column delete
#26 31 37 38 39 43 44 64 60 59 58 56 49 47 9 20 id

str(bankrupt)
bankrupt$Attr9 <- NULL
bankrupt$Attr20 <- NULL
bankrupt$Attr47 <- NULL
bankrupt$Attr49 <- NULL
bankrupt$Attr56 <- NULL
bankrupt$Attr58 <- NULL
bankrupt$Attr59 <- NULL
bankrupt$Attr60 <- NULL
bankrupt$Attr64 <- NULL
bankrupt$Attr44 <- NULL
bankrupt$Attr43 <- NULL
bankrupt$Attr39 <- NULL
bankrupt$Attr38 <- NULL
bankrupt$Attr37 <- NULL
bankrupt$Attr31 <- NULL
bankrupt$Attr26 <- NULL


########## logistic regression algorithm##########
library('caTools')
filter <- sample.split(bankrupt$Attr4, SplitRatio = 0.7)
bankrupt.train <- subset(bankrupt, filter == T)
bankrupt.test <- subset(bankrupt, filter == F)

dim(bankrupt)
dim(bankrupt.test)
dim(bankrupt.train)

bankrupt.model <- glm(class~.,family = binomial(link = 'logit'), data = bankrupt.train)
summary(bankrupt.model)

#����� �� ����� ������ �� �-test set
predict.bankrupt.test <- predict(bankrupt.model, newdata = bankrupt.test, type = 'response')

#����� ����� ���� ����� ���, ����� ������ ����� �-0.5 ���� �-0.5
confusion.matrix <- table(predict.bankrupt.test>0.5, bankrupt.test$class)

precision <- confusion.matrix[2,2] / (confusion.matrix[2,1]+confusion.matrix[2,2])
recall <- confusion.matrix[2,2] / (confusion.matrix[1,2]+confusion.matrix[2,2])

precision
recall



##########decition tree##########

#Build model 

#install.packages('rpart')
library(rpart)
#install.packages('rpart.plot')
library(rpart.plot)



#run the model 
model <- rpart(class~.-Attr9-Attr20-Attr47-Attr49-Attr56-Attr58-Attr59-Attr60-Attr64-Attr44-Attr43-Attr39-Attr38-Attr37-Attr31-Attr26,bankrupt.train) 

model <- rpart(class~.,bankrupt.train) 

rpart.plot(model, box.palette = "RdBu", shadow.col = "grey", nn=TRUE)

predict.prob <- predict(model,bankrupt.test)

predict.prob.yes <- predict.prob[,'1']

prediciton <- predict.prob.yes > 0.5

actual <- bankrupt.test$class

cf <- table(prediciton,actual)

summary(bankrupt)

precision <- cf['TRUE','1']/(cf['TRUE','1'] + cf['TRUE','0'])
recall <- cf['TRUE','1']/(cf['TRUE','1'] + cf['FALSE','1'])
#recall 0.6015 percition-0.7475

#train fitting
predict.prob1 <- predict(model,bankrupt.train)

predict.prob.yes1 <- predict.prob1[,'1']

prediciton1 <- predict.prob.yes1 > 0.5

actual1 <- bankrupt.train$class

cf1 <- table(prediciton1,actual1)

summary(bankrupt)

precision1 <- cf1['TRUE','1']/(cf1['TRUE','1'] + cf1['TRUE','0'])
recall1 <- cf1['TRUE','1']/(cf1['TRUE','1'] + cf1['FALSE','1'])
#recall-0.6347 percition-0.8994

total_errors <- (cf['TRUE','0']+cf['FALSE','1'])/dim(bankrupt.test)[1]
#0.042
total_errors1 <- (cf1['TRUE','0']+cf1['FALSE','1'])/dim(bankrupt.train)[1]
#0.0326
#������� ��� ���� ����� �� �� ���� ����� ��� ���� ������

#Computing base level 
number.bankrupt <- dim(bankrupt[bankrupt$class=='1',])[1]
base_level <- number.bankrupt/dim(bankrupt)[1]
#Model improved errors from 7% in base level to 4% with model 




##########naive base algorithm##########

#Divide into trainig set and test set 

dim(bankrupt.train)
#[1] 4137   49

dim(bankrupt.test)
#[1] 1773   49

#install.packages('e1071')
library(e1071)

#modelNB
modelNB <- naiveBayes(bankrupt.train$class ~ ., data = bankrupt.train)
prediction <- predict(modelNB, bankrupt.test,type = 'raw')
prediction_NB <- prediction[,'1']
actual_NB <- bankrupt.test$class 
predicted_NB <- prediction_NB > 0.5

#Confusion matrix
cfNB <- table(predicted_NB,actual_NB)
#             actual_NB
#predicted_NB    0    1
#FALSE          42    4
#TRUE          1608  119

precision <- cfNB[2,2]/( cfNB[2,2]+ cfNB[2,1]) 
#[1] 0.06890562

recall <- cfNB[2,2]/( cfNB[2,2]+ cfNB[1,2])   
#[1] 0.9674797

total_errors_NB<- (cfNB['TRUE','0']+cfNB['FALSE','1'])/dim(bankrupt.test)[1]
#[1] 0.9091935


#Computing base level 
number.bankrupt <- dim(bankrupt[bankrupt$class=='1',])[1]
base_level <- number.bankrupt/dim(bankrupt)[1]
#Model improved errors from 7% in base level to 4% with model 




bankrupt.test$class
str(bankrupt.test$class)
summary(bankrupt.test$class)
levels(bankrupt.test$class) <- c("FALSE", "TRUE")

# ROC chart
#install.packages('pROC')
library(pROC)

rocCurveNB <- roc(bankrupt.test$class, prediction_NB, direction = "<", levels = c("TRUE","FALSE"))
rocCurveTR <- roc(bankrupt.test$class, predict.prob.yes, direction = "<", levels = c("FALSE","TRUE"))
rocCurveGLM <- roc(bankrupt.test$class, predict.bankrupt.test, direction = "<", levels = c("TRUE","FALSE"))
#��� ����� ���� ���� ���� ���, ����� ��������� ��� ��� ���� ����� �� ��� ����� ����� ���� ����� �� ��������� ���� ��� ����

plot(rocCurveNB, col ='red', main = 'ROC chart')
par(new=TRUE)
#�������� ��� ������ ��� ����� 2 ����� �� ���� ���
plot(rocCurveTR, col ='blue', main = 'ROC chart')
#���� ����� ���� ������ ���� ���� ��� �� ��� ��� ��� �����
plot(rocCurveGLM, col ='green',  'ROC chart')

auc(rocCurveNB)
auc(rocCurveTR)
auc(rocCurveGLM)


#auc ������� ������� �� ����(=����) �� ����� ����� ���� ������ �����
#��� ���� ����� �� ����� rocCurveTR ���� ��� ���� ���� �-rocCurveNB ���� ��� ��� ����� ����

